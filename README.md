This is an example project used to demonstrate how to use [GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html)
to improve the process of developing a game.

Start by checking out the [project documentation](doc/index.md).
#changes-done-here-readme
